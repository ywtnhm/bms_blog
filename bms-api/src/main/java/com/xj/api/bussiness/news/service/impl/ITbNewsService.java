package com.xj.api.bussiness.news.service.impl;

import com.baomidou.mybatisplus.service.IService;
import com.xj.common.bussiness.article.entity.TbArticle;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author xj
 * @since 2017-01-05
 */
public interface ITbNewsService extends IService<TbArticle> {
	
}
