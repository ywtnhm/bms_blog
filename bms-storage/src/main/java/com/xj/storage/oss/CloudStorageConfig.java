package com.xj.storage.oss;

import java.io.Serializable;

public class CloudStorageConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    private int appid;

    private String domain;

    private String prefix;

    private String endpoint;

    private String bucket;

    private String accessKey;

    private String secretKey;

    public CloudStorageConfig(){

    }

    public CloudStorageConfig(String domain,String prefix,String endpoint,String bucket,String accessKey,String secretKey){
        this.domain = domain;
        this.prefix = prefix;
        this.endpoint = endpoint;
        this.bucket = bucket;
        this.accessKey = accessKey;
        this.secretKey = secretKey;
    }

    public CloudStorageConfig(int appid,String domain,String prefix,String endpoint,String bucket,String accessKey,String secretKey){
        this.appid = appid;
        this.domain = domain;
        this.prefix = prefix;
        this.endpoint = endpoint;
        this.bucket = bucket;
        this.accessKey = accessKey;
        this.secretKey = secretKey;
    }

    public int getAppid() {
        return appid;
    }

    public void setAppid(int appid) {
        this.appid = appid;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }


}

