package com.xj.common.bussiness.category.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.xj.common.bussiness.category.entity.TbCategory;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2017-06-28
 */
public interface TbCategoryMapper extends BaseMapper<TbCategory> {

	public List<TbCategory> selectCategoryPage(Pagination page,@Param("ew") Wrapper<TbCategory> wrapper);
	
	public TbCategory selectCategoryById(@Param("id") Integer id);
	
	public List<TbCategory> selectCategoryList(Pagination page,@Param("categoryId") Integer categoryId);
}