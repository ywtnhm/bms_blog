package com.xj.common.bussiness.article.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.xj.common.bussiness.article.entity.TbArticle;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author xj
 * @since 2017-01-05
 */
public interface TbArticleMapper extends BaseMapper<TbArticle> {

	public List<TbArticle> selectArticlePage(Pagination page,@Param("ew") Wrapper<TbArticle> wrapper);
}