package com.xj.common.base.common.constant;

/**
 * Created by laizhilong on 2017/7/25.
 */
public interface uploadConstant {

    /**
     * 存储类型  1.七牛 2.阿里 3.腾讯
     */
   int qiniu_type = 1;
   int aliyun_type = 2;
   int cos_type = 3;

}
