package com.xj.admin.bussiness.article.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xj.admin.bussiness.article.service.ITbArticleService;
import com.xj.common.bussiness.article.entity.TbArticle;
import com.xj.common.bussiness.article.mapper.TbArticleMapper;

/**
 * <p>
 * 栏目管理  服务实现类
 * </p>
 *
 * @author xj
 * @since 2017-01-05
 */
@Service
public class TbArticleServiceImpl extends ServiceImpl<TbArticleMapper, TbArticle> implements ITbArticleService {

	@Autowired
	private TbArticleMapper articleMapper;
	
	@Override
	public Page<TbArticle> selectArticlePage(Page<TbArticle> page, Wrapper<TbArticle> wrapper) {
		page.setRecords(articleMapper.selectArticlePage(page, wrapper));
		return page;
	}
	
}
