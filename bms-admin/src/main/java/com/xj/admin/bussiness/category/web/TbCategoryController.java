package com.xj.admin.bussiness.category.web;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.plugins.Page;
import com.feilong.core.Validator;
import com.xj.admin.base.dict.service.ITbDictService;
import com.xj.admin.base.index.web.BaseController;
import com.xj.admin.bussiness.category.service.ITbCategoryService;
import com.xj.admin.util.JsonUtil;
import com.xj.admin.util.dtgrid.model.Pager;
import com.xj.common.base.common.bean.AbstractBean;
import com.xj.common.base.common.exception.EnumSvrResult;
import com.xj.common.bussiness.category.entity.TbCategory;

/**
 * <p>
 *   前端控制器
 * </p>
 *
 * @author xj
 * @since 2017-06-28
 */
@Controller
@RequestMapping("/category/")
public class TbCategoryController extends BaseController {

	@Autowired
	private ITbCategoryService categoryService;
	
	@Autowired
	private ITbDictService dictService;
	
	@GetMapping("listUI")
    public String listUI() {
		return "category/list";
    }
	
	@RequestMapping("form")
    public String form(Map<String,Object> map,Integer pid) {
		if(Validator.isNullOrEmpty(pid)){
			pid = 1;
		}
		map.put("parentCategory", categoryService.selectById(pid));
		makeCommon(map);
		return "category/form";
    }
    
    @PostMapping("list")
	@ResponseBody
    public Object list(String gridPager) {
		Pager pager = JsonUtil.getObjectFromJson(gridPager, Pager.class);
		Map<String, Object> parameters = null;
		if(Validator.isNullOrEmpty(pager.getParameters())){
			parameters = new HashMap<>();
		}else{
			parameters = pager.getParameters();
		}
		Page<TbCategory> list = null;
		if(Validator.isNotNullOrEmpty(parameters.get("pid"))){
			list = categoryService.selectCategoryList(new Page<TbCategory>(pager.getNowPage(), pager.getPageSize()), Integer.parseInt(parameters.get("pid").toString())); 
		}else{
			list = categoryService.selectPage(new Page<TbCategory>(pager.getNowPage(), pager.getPageSize()));
		}
		makeGridResult(parameters, pager, list);
		return parameters;
    }
    
    @SuppressWarnings("unchecked")
	@GetMapping("listTree")
	@ResponseBody
    public Object listTree() {
		return categoryService.selectList(Condition.create().orderBy("sort", false));
    }
	
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public AbstractBean add(TbCategory tbCategory){
		if(tbCategory.getPid()!=0){
			TbCategory parentDept = categoryService.selectById(tbCategory.getPid());
			tbCategory.setPids(parentDept.getPids()+tbCategory.getPid()+",");
		}
		tbCategory.setIsNav(Validator.isNullOrEmpty(tbCategory.getIsNav())?2:tbCategory.getIsNav());
		if(Validator.isNotNullOrEmpty(tbCategory.getId())){
			tbCategory.setUpdateTime(new Date());
			tbCategory.setUpdateUser(getUserEntity().getAccountName());
			categoryService.updateById(tbCategory);
		}else{
			tbCategory.setCreateTime(new Date());
			tbCategory.setCreateUser(getUserEntity().getAccountName());
			categoryService.insert(tbCategory);
		}
		return success();
	}
	
	@RequestMapping(value="{id}/delete",method=RequestMethod.DELETE)
	@ResponseBody
    public AbstractBean delete(@PathVariable(required=true) Integer id) {	
		boolean result = false;
		try {
			result = categoryService.deleteById(id);
		} catch (Exception e) {
			return fail(EnumSvrResult.ERROR_DELETE_DEPT);
		}
		if(result){
			return success();
		}else{
			return error();
		}
    }	
	
	@RequestMapping(value="{id}/select",method=RequestMethod.GET)
    public String select(Map<String,Object> map,@PathVariable Integer id) {	
		TbCategory TbCategory = categoryService.selectCategoryById(id);
		map.put("record",TbCategory);
		makeCommon(map);
		return "category/edit";
    }	
	@GetMapping("toSelectTree")
    public String toSelectTree() {	
		return "category/selectTree";
    }	
	
	@SuppressWarnings("unchecked")
	private void makeCommon(Map<String,Object> map){
		map.put("categoryTypes",  dictService.selectList(Condition.create().eq("code", "categoryType").ne("num", 0).orderBy("num")));
	}
}
