package com.xj.admin.bussiness.category.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.xj.admin.bussiness.category.service.ITbCategoryService;
import com.xj.common.bussiness.category.entity.TbCategory;
import com.xj.common.bussiness.category.mapper.TbCategoryMapper;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xj
 * @since 2017-06-28
 */
@Service
public class TbCategoryServiceImpl extends ServiceImpl<TbCategoryMapper, TbCategory> implements ITbCategoryService {
	
	@Autowired
	private TbCategoryMapper categoryMapper;

	@Override
	public Page<TbCategory> selectCategoryPage(Page<TbCategory> page,Wrapper<TbCategory> wrapper) {
		page.setRecords(categoryMapper.selectCategoryPage(page, wrapper));
		return page;
	}

	@Override
	public TbCategory selectCategoryById(Integer id) {
		return categoryMapper.selectCategoryById(id);
	}

	@Override
	public Page<TbCategory> selectCategoryList(Page<TbCategory> page, Integer categoryId) {
		page.setRecords(categoryMapper.selectCategoryList(page, categoryId));
		return page;
	}

}
