package com.xj.admin.bussiness.article.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.xj.common.bussiness.article.entity.TbArticle;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author xj
 * @since 2017-01-05
 */
public interface ITbArticleService extends IService<TbArticle> {
	
	Page<TbArticle> selectArticlePage(Page<TbArticle> page,Wrapper<TbArticle> wrapper);
}
