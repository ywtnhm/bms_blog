package com.xj.admin.bussiness.category.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.xj.common.bussiness.category.entity.TbCategory;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xj
 * @since 2017-06-28
 */
public interface ITbCategoryService extends IService<TbCategory> {
	
	Page<TbCategory> selectCategoryPage(Page<TbCategory> page,Wrapper<TbCategory> wrapper);
	
	Page<TbCategory> selectCategoryList(Page<TbCategory> page,Integer categoryId);
	
	TbCategory selectCategoryById(Integer id);
}
