package com.xj.admin.bussiness.article.web;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.plugins.Page;
import com.feilong.core.Validator;
import com.feilong.core.bean.ConvertUtil;
import com.xj.admin.base.index.web.BaseController;
import com.xj.admin.bussiness.article.service.ITbArticleService;
import com.xj.admin.bussiness.category.service.ITbCategoryService;
import com.xj.admin.util.dtgrid.model.Pager;
import com.xj.common.base.common.bean.AbstractBean;
import com.xj.common.base.common.exception.EnumSvrResult;
import com.xj.common.base.common.util.JsonUtil;
import com.xj.common.bussiness.article.entity.TbArticle;

/**
 * <p>
 * 栏目管理  前端控制器
 * </p>
 *
 * @author xj
 * @since 2017-01-05
 */
@Controller
@RequestMapping("/article/")

public class TbArticleController extends BaseController {

	@Autowired
	private ITbArticleService articleService;
	
	@Autowired
	private ITbCategoryService categoryService;
	
	@GetMapping("listUI")
    public String listUI() {
		return "article/list";
    }
	
	@SuppressWarnings("unchecked")
	@PostMapping("list")
	@ResponseBody
    public Object list(String gridPager) {
		Pager pager = JsonUtil.getObjectFromJson(gridPager, Pager.class);
		Map<String, Object> parameters = null;
		if(Validator.isNullOrEmpty(pager.getParameters())){
			parameters = new HashMap<>();
		}else{
			parameters = pager.getParameters();
		}
		Page<TbArticle> list = articleService.selectArticlePage(new Page<TbArticle>(pager.getNowPage(), pager.getPageSize()), Condition.create().allEq(parameters).orderBy("id",false));
		makeGridResult(parameters, pager, list);
		return parameters;
    }
	
	@GetMapping("form")
    public String form(Map<String,Object> map,@RequestParam Integer categoryId) {
		makeCommon(map, categoryId);
		return "article/form";
    }
	
	@PostMapping("save")
	@ResponseBody
	public AbstractBean add(TbArticle tbnews){
		AbstractBean bean = new AbstractBean();
			if(tbnews.getId()==null){
				tbnews.setCreateTime(new Date());
				tbnews.setCreateUser(getUserEntity().getAccountName());
				tbnews.setUpdateTime(new Date());
			}else{
				tbnews.setUpdateTime(new Date());
				tbnews.setUpdateUser(getUserEntity().getAccountName());
			}
			tbnews.setIsShow(tbnews.getIsShow()==null?"1":"2");
			if(!articleService.insertOrUpdate(tbnews)){
				bean.setStatus(EnumSvrResult.ERROR.getVal());
				bean.setMessage(EnumSvrResult.ERROR.getName());
			}
		return bean;
	}
	
	@DeleteMapping("{id}/delete")
	@ResponseBody
    public AbstractBean delete(@PathVariable(required=true) Integer id) {	
		if(!articleService.deleteById(id)){
			return fail(EnumSvrResult.ERROR);
		}
		return success();
    }	
	
	@GetMapping("{id}/select")
    public String select(Map<String,Object> map,@PathVariable(required=true) Integer id) {	
		TbArticle tbnews = articleService.selectById(id);
		map.put("record", tbnews);
		makeCommon(map, tbnews.getCategoryId());
		return "article/edit";
    }	
	
	@DeleteMapping("{ids}/deleteBatch")
	@ResponseBody
	public AbstractBean deleteBatch(@PathVariable(required=true) String ids) {
		if(!articleService.deleteBatchIds(ConvertUtil.toList(ids.split(",")))){
			return fail(EnumSvrResult.ERROR);
		}
		return success();
	}
	
	private void makeCommon(Map<String,Object> map,Integer categoryId){
		map.put("category", categoryService.selectById(categoryId));
	}
}
