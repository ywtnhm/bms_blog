package com.xj.admin.base.upload.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.xj.admin.base.config.entity.TbConfig;
import com.xj.admin.base.config.service.ITbConfigService;
import com.xj.common.base.common.constant.uploadConstant;
import com.xj.common.base.common.constant.defaultConstan;
import com.xj.storage.oss.AliyunCloudStorageService;
import com.xj.storage.oss.CloudStorageConfig;
import com.xj.storage.oss.QcloudCloudStorageService;
import com.xj.storage.oss.QiniuCloudStorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xj.admin.base.index.web.BaseController;
import com.xj.admin.util.ImageUploadUtil;
import com.xj.common.base.common.bean.AbstractBean;
import com.xj.common.base.common.exception.EnumSvrResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 用户账户表 前端控制器
 * </p>
 *
 * @author xj
 * @since 2016-12-20
 */
@Controller
@RequestMapping("/upload/")
public class UploadController extends BaseController{
	private Logger logger = LogManager.getLogger(UploadController.class.getName());

	@Autowired
	private ITbConfigService configService;

	@RequestMapping("ckUploadImg")
	public void ckUpdateImg(HttpServletRequest request,HttpServletResponse response){
			String DirectoryName = "upload/ck/";
		try{	
			ImageUploadUtil.ckeditor(request, response, DirectoryName);
			
		}catch(Exception e){
			logger.error("CK上傳失敗：{}",e.getMessage());
		}
		
	}
	
//	@RequestMapping("uploadImg")
//	@ResponseBody
//	public AbstractBean updateImg(HttpServletRequest request){
//			String DirectoryName = "upload/other/";
//		try{
//			String fileName = ImageUploadUtil.upload(request, DirectoryName);
//			return json(DirectoryName+fileName);
//		}catch(Exception e){
//			logger.error("上傳失敗：{}",e.getMessage());
//			return fail(EnumSvrResult.ERROR_UPLOAD_IMG);
//		}
//	}

	@RequestMapping("uploadImg")
	@ResponseBody
	public AbstractBean updateImg(@RequestParam("upFile") MultipartFile file, HttpServletRequest request){
		String url = "";
		try{
			EntityWrapper<TbConfig> entityWrapper = new EntityWrapper<TbConfig>();
			List<TbConfig> configs = this.configService.selectList(entityWrapper);
			for(TbConfig config : configs){
				if(config.getIsDefault() == defaultConstan.DEFAULT_YES && config.getType() == uploadConstant.qiniu_type){//七牛存储
					CloudStorageConfig  cloudStorageConfig = new CloudStorageConfig();
					cloudStorageConfig.setAccessKey(config.getAccessKey());
					cloudStorageConfig.setBucket(config.getBucket());
					cloudStorageConfig.setDomain(config.getDomain());
					cloudStorageConfig.setEndpoint(config.getEndpoint());
					cloudStorageConfig.setPrefix(config.getCosPath());
					cloudStorageConfig.setSecretKey(config.getSecretKey());
					QiniuCloudStorageService qiniuCloudStorageService = new QiniuCloudStorageService(cloudStorageConfig);
					String cosFilePath = cloudStorageConfig.getPrefix() + "/" + file.getOriginalFilename();
					url = qiniuCloudStorageService.upload(file.getBytes(),cosFilePath);
				}else if(config.getIsDefault() == defaultConstan.DEFAULT_YES  && config.getType() == uploadConstant.aliyun_type){//阿里云oss存储
					CloudStorageConfig  cloudStorageConfig = new CloudStorageConfig();
					cloudStorageConfig.setAccessKey(config.getAccessKey());
					cloudStorageConfig.setBucket(config.getBucket());
					cloudStorageConfig.setDomain(config.getDomain());
					cloudStorageConfig.setEndpoint(config.getEndpoint());
					cloudStorageConfig.setPrefix(config.getCosPath());
					cloudStorageConfig.setSecretKey(config.getSecretKey());
					AliyunCloudStorageService qiniuCloudStorageService = new AliyunCloudStorageService(cloudStorageConfig);
					String cosFilePath = cloudStorageConfig.getPrefix() + "/" + file.getOriginalFilename();
					url = qiniuCloudStorageService.upload(file.getBytes(),cosFilePath);
				}else if(config.getIsDefault() == defaultConstan.DEFAULT_YES  && config.getType() == uploadConstant.cos_type){//腾讯cos存储
					CloudStorageConfig  cloudStorageConfig = new CloudStorageConfig();
					cloudStorageConfig.setAccessKey(config.getAccessKey());
					cloudStorageConfig.setBucket(config.getBucket());
					cloudStorageConfig.setDomain(config.getDomain());
					cloudStorageConfig.setEndpoint(config.getEndpoint());
					cloudStorageConfig.setPrefix(config.getCosPath());
					cloudStorageConfig.setSecretKey(config.getSecretKey());
					cloudStorageConfig.setAppid(config.getAppid());
					QcloudCloudStorageService qiniuCloudStorageService = new QcloudCloudStorageService(cloudStorageConfig);
					String cosFilePath = cloudStorageConfig.getPrefix() + "/" + file.getOriginalFilename();
					url = qiniuCloudStorageService.upload(file.getBytes(),cosFilePath);

				}
			}
			System.out.println(url);
			return json(url);
		}catch(Exception e){
			logger.error("上傳失敗：{}",e.getMessage());
			return fail(EnumSvrResult.ERROR_UPLOAD_IMG);
		}
	}
}
