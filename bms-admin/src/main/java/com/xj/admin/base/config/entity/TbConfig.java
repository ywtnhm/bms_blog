package com.xj.admin.base.config.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 第三方存储
 * @author laizhilong
 *
 */
@TableName("tb_config")
public class TbConfig extends Model<TbConfig>{
	
	private Integer id;
	
	private Integer appid;
	
	private String domain;
	
	@TableField(value = "cos_path")
	private String cosPath;
	
	private String endpoint;
	
	private String bucket;
	
	@TableField(value = "access_key")
	private String accessKey;
	
	@TableField(value = "secret_key")
	private String secretKey;
	
	@TableField(value = "is_default")
	private Integer isDefault;
	
	private Integer type;
	
	@TableField(value="create_time")
	private Date createTime;
	/**
	 * 
	 */
	@TableField(value="create_user")
	private String createUser;
	/**
	 * 
	 */
	@TableField(value="update_time")
	private Date updateTime;
	/**
	 * 
	 */
	@TableField(value="update_user")
	private String updateUser;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAppid() {
		return appid;
	}

	public void setAppid(Integer appid) {
		this.appid = appid;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getCosPath() {
		return cosPath;
	}

	public void setCosPath(String cosPath) {
		this.cosPath = cosPath;
	}
	

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public Integer getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}



	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}

}
