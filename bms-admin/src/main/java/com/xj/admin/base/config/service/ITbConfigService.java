package com.xj.admin.base.config.service;

import com.baomidou.mybatisplus.service.IService;
import com.xj.admin.base.config.entity.TbConfig;

public interface ITbConfigService extends IService<TbConfig>{

}
