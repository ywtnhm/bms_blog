package com.xj.admin.base.config.web;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.feilong.core.Validator;
import com.xj.admin.base.config.entity.TbConfig;
import com.xj.admin.base.config.service.ITbConfigService;
import com.xj.admin.base.index.web.BaseController;
import com.xj.admin.util.JsonUtil;
import com.xj.admin.util.dtgrid.model.Pager;
import com.xj.common.base.common.bean.AbstractBean;
import com.xj.common.base.common.exception.EnumSvrResult;

@Controller
@RequestMapping("/config/")
public class TbConfigController extends BaseController{
	
	@Autowired
	private ITbConfigService configService;
	
	
	@GetMapping("listUI")
    public String listUI() {
		return "config/list";
    }

	@PostMapping("list")
	@ResponseBody
    public Object list(String gridPager) {
		Pager pager = JsonUtil.getObjectFromJson(gridPager, Pager.class);
		Map<String, Object> parameters = null;
		if(Validator.isNullOrEmpty(pager.getParameters())){
			parameters = new HashMap<>();
		}else{
			parameters = pager.getParameters();
		}
		Page<TbConfig> list = configService.selectPage(new Page<TbConfig>(pager.getNowPage(), pager.getPageSize()), Condition.create().allEq(parameters).orderBy("create_time", false));
		makeGridResult(parameters, pager, list);
		return parameters;
    }
	
	
	@GetMapping("form")
    public String form(Map<String,Object> map) {
		return "config/form";
    }
	
	@PostMapping("save")
	@ResponseBody
	public AbstractBean add(TbConfig tbConfig){
		AbstractBean bean = new AbstractBean();
		EntityWrapper<TbConfig> wrapper = new EntityWrapper<TbConfig>();
		List<TbConfig> configs = this.configService.selectList(wrapper);
		for(TbConfig temp :configs){
			if(tbConfig.getIsDefault() == 1){
				temp.setIsDefault(2);
				this.configService.updateById(temp);
			}
		}
		if (tbConfig.getId() == null) {
			tbConfig.setCreateTime(new Date());
			tbConfig.setCreateUser(getUserEntity().getAccountName());
		} else {
			tbConfig.setUpdateTime(new Date());
			tbConfig.setUpdateUser(getUserEntity().getAccountName());
		}
		tbConfig.setIsDefault(Integer.parseInt(tbConfig.getIsDefault() == null ? "2" : "1"));
		if (!configService.insertOrUpdate(tbConfig)) {
			bean.setStatus(EnumSvrResult.ERROR.getVal());
			bean.setMessage(EnumSvrResult.ERROR.getName());
			}
		return bean;
	}
	
	@DeleteMapping("{id}/delete")
	@ResponseBody
    public AbstractBean delete(@PathVariable(required=true) Integer id) {	
		if(!configService.deleteById(id)){
			return fail(EnumSvrResult.ERROR);
		}
		return success();
    }	
	
	@GetMapping("{id}/select")
    public String select(Map<String,Object> map,@PathVariable(required=true) Integer id) {	
		TbConfig config = this.configService.selectById(id);
		map.put("record", config);
		return "config/edit";
    }	
	
	@PostMapping("{id}/settingConfig")
	@ResponseBody
	public AbstractBean settingConfig(@PathVariable(required=true) Integer id){
		EntityWrapper<TbConfig> wrapper = new EntityWrapper<TbConfig>();
		List<TbConfig> configs = this.configService.selectList(wrapper);
		for(TbConfig temp :configs){
			if(temp.getIsDefault() == 1){
				temp.setIsDefault(2);
				this.configService.updateById(temp);
			}
		}
		TbConfig record = this.configService.selectById(id);
		record.setIsDefault(1);
		record.setUpdateTime(new Date());
		record.setUpdateUser(getUserEntity().getAccountName());
		this.configService.updateById(record);
		return success();
	}
		
	
}
