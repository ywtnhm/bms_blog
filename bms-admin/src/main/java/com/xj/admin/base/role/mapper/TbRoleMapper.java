package com.xj.admin.base.role.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xj.admin.base.role.entity.TbRole;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author xj
 * @since 2016-12-20
 */
public interface TbRoleMapper extends BaseMapper<TbRole> {
	
}