package com.xj.admin.base.config.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xj.admin.base.config.entity.TbConfig;

public interface TbConfigMapper extends BaseMapper<TbConfig>{

}
