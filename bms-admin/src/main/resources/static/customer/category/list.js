
var dict;
var grid ;
$(function() {
	dict = getDicts('categoryType');
	var dtGridColumns = [{
	    id : 'name',
	    title : '栏目',
	    type : 'string',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;'
	},{
	    id : 'coverImg',
	    title : '封面',
	    type : 'string',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;',
	    resolution:function(value){
	        return '<img class="am-radius" src="'+value+'" class="tpl-table-line-img" alt="" height="50px"/>';
	     }
	},{
	    id : 'type',
	    title : '类型',
	    type : 'string',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;',
	    codeTable:dict.categoryType

	},{
	    id : 'isNav',
	    title : '导航显示',
	    type : 'int',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;',
	    resolution:function(value){
	    	if(value==1){
	    		return '<span class="am-badge am-badge-success am-round">是</span>';
	    	}else{
	    		return '<span class="am-badge am-badge-danger am-round">否</span>';
	    	}
	        
	     }
	},{
	    id : 'createTime',
	    title : '创建时间',
	    type : 'date',
	    format:'yyyy-MM-dd hh:mm:ss', 
	    otype:'time_stamp_ms', 
	    oformat:'string',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;'
	}];

	var dtGridOption = {
	    lang : 'zh-cn',
	    ajaxLoad : true,
	    checkWidth :'37px',
	    check : true,
	    loadURL : 'category/list',
	    columns : dtGridColumns,
	    gridContainer : 'dtGridContainer',
	    toolbarContainer : 'dtGridToolBarContainer',
	    tools : '',
	    pageSize : 10,
	    pageSizeLimit : [10, 20, 30]
	};
	grid = $.fn.dlshouwen.grid.init(dtGridOption);
	grid.load();
	$('select').selected();
	var setting = {
			data: {
				simpleData: {
					enable: true,
					pIdKey: "pid",
				}
			},
			callback: {
				onClick: zTreeOnClick
			}
	};
	initCategoryTree(setting);
});


function zTreeOnClick(event, treeId, treeNode) {
	$("#pid").val(treeNode.id);
	search(treeNode.id);
};
function search(pid){
	  grid.parameters = new Object();
	  grid.parameters['pid'] = pid;
	  grid.refresh(true);
}

function btnDel(){
	   var rows = grid.getCheckedRecords();
 if (rows.length == 1) {
	   del('category/'+rows[0].id+'/delete',search);
 }else{
	   layer.msg("你没有选择行或选择了多行数据", {
         icon : 0
     });
 }
}
function btnEdit(){
	   var rows = grid.getCheckedRecords();
 if (rows.length == 1) {
 	loadPage('category/'+rows[0].id+'/select');
 }else{
	   layer.msg("你没有选择行或选择了多行数据", {
         icon : 0
     });
 }
}
function toAdd(){
	 loadPage('category/form?pid='+$('#pid').val());
}

