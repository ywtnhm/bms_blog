var dtGridColumns = [{
    id : 'domain',
    title : '访问域名',
    type : 'string',
    columnClass : 'text-center',
    headerStyle : 'color:grey;'
},
//{
//    id : 'endpoint',
//    title : '姓名',
//    type : 'string',
//    columnClass : 'text-center',
//    headerStyle : 'color:grey;'
//},
{
    id : 'bucket',
    title : 'bucket名称',
    type : 'string',
    columnClass : 'text-center',
    headerStyle : 'color:grey;',
   
},
//{
//    id : 'cosPath',
//    title : '文件路径',
//    type : 'string',
//    columnClass : 'text-center',
//    headerStyle : 'color:grey;',
//   
//},
{
    id : 'isDefault',
    title : '是否默认',
    type : 'number',
    columnClass : 'text-center',
    headerStyle : 'color:grey;',
    resolution:function(value){
        if(value==1){
        	return '是';
        }else{
        	return '否';
        }
    }
},{
    id : 'type',
    title : '类型',
    type : 'string',
    columnClass : 'text-center',
    headerStyle : 'color:grey;',
    resolution:function(value){
        if(value==1){
        	return '七牛';
        }else if(value == 2){
        	return '阿里';
        }else if(value == 3){
        	return '腾讯';
        }
    }
},
//{
//    id : 'accessKey',
//    title : 'accessKey',
//    type : 'string',
//    columnClass : 'text-center',
//    headerStyle : 'color:grey;'
//},
//{
//    id : 'secretKey',
//    title : 'secretKey',
//    type : 'string',
//    columnClass : 'text-center',
//    headerStyle : 'color:grey;'
//},
//{
//    id : 'ctime',
//    title : '创建时间',
//    type : 'date',
//    format:'yyyy-MM-dd hh:mm:ss', 
//    otype:'time_stamp_ms', 
//    oformat:'string',
//    columnClass : 'text-center',
//    headerStyle : 'color:grey;'
//},
{
    id : 'operation',
    title : '操作',
    type : 'string',
    columnClass : 'text-center tpl-table-black-operation',
    headerStyle : 'color:grey;',
    resolution:function(value, record, column, grid, dataNo, columnNo){
    	 var editCheck = $("input[name=edit_check]").val();
         var delCheck = $("input[name=delete_check]").val();
         var setDefaultCheck = $("input[name=setDefault_check]").val();
        var content = '';
        if(editCheck == 1){
        	content += '<a href="#" onclick="loadPage(\'config/'+record.id+'/select\');" > <i class="am-icon-pencil"></i> 编辑</a> ';
        }
        content += '  ';
        if(delCheck == 1){
        	content += '<a href="#" onclick="delConfig('+record.id+');" class="tpl-table-black-operation-del del-btn" ><i class="am-icon-trash"></i> 删除</a>';
        }
        content += '  ';
        if(setDefaultCheck == 1){
        	if(record.isDefault == 2){
        	content += '<a href="#" onclick="settingConfig('+record.id+');" class="tpl-table-black-success am-btn"><i class="am-icon-cogs"></i> 设置默认</a>';
        
        	}
        }
        return content;
    }
}];

var dtGridOption = {
    lang : 'zh-cn',
    ajaxLoad : true,
    check : true,
    checkWidth :'37px',
    loadURL : 'config/list',
    columns : dtGridColumns,
    gridContainer : 'dtGridContainer',
    toolbarContainer : 'dtGridToolBarContainer',
    tools : '',
    pageSize : 10,
    pageSizeLimit : [10, 20, 30]
};
var grid = $.fn.dlshouwen.grid.init(dtGridOption);


$(function() {
	grid.load();
	$('select').selected();
});



function delConfig(id){
	del("config/"+id+"/delete",search);
}

function settingConfig(id){
	setting("config/"+id+"/settingConfig",search);
}

function search(){
	  grid.parameters = new Object();
	  //grid.parameters['name'] = $("#name").val();
	  grid.refresh(true);
}
