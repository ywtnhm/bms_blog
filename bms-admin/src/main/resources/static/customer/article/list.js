var grid;
$(function() {
	var dtGridColumns = [{
	    id : 'id',
	    title : '编号',
	    type : 'int',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;'
	},{
	    id : 'coverImg',
	    title : '封面',
	    type : 'string',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;',
	    resolution:function(value){
	       return '<img src="'+value+'" class="tpl-table-line-img" alt=""/>';
	    }
	},{
	    id : 'title',
	    title : '标题',
	    type : 'string',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;'
	},{
	    id : 'descs',
	    title : '描述',
	    type : 'string',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;'
	},{
	    id : 'categoryName',
	    title : '栏目',
	    type : 'string',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;',
	    resolution:function(value){
	       return '<span class="am-badge am-badge-secondary am-round">'+value+'</span>';
	    }
	    
	},{
	    id : 'isShow',
	    title : '状态',
	    type : 'string',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;',
	    resolution:function(value){
	        if(value==1){
	        	return '<span class="am-badge am-badge-success am-round">发布</span>';
	    	}else{
	    		return '<span class="am-badge am-badge-danger am-round">草稿</span>';
	    	}
	    }
	},{
	    id : 'createUser',
	    title : '创建者',
	    type : 'string',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;',
	    hideType : 'xs|sm|md|lg'
	},{
	    id : 'createTime',
	    title : '创建时间',
	    type : 'date',
	    format:'yyyy-MM-dd hh:mm:ss', 
	    otype:'time_stamp_ms', 
	    oformat:'string',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;',
	    hideType : 'xs|sm|md|lg'
	},{
	    id : 'updateTime',
	    title : '更新时间',
	    type : 'date',
	    format:'yyyy-MM-dd hh:mm:ss', 
	    otype:'time_stamp_ms', 
	    oformat:'string',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;',
	    hideType : 'xs|sm|md|lg'
	},{
	    id : 'updateUser',
	    title : '更新人',
	    type : 'string',
	    columnClass : 'text-center',
	    headerStyle : 'color:grey;',
	    hideType : 'xs|sm|md|lg'
	},{
	    id : 'operation',
	    title : '操作',
	    type : 'string',
	    columnClass : 'text-center tpl-table-black-operation',
	    headerStyle : 'color:grey;',
	    resolution:function(value, record, column, grid, dataNo, columnNo){
	        var content = '';
	        content += '<a href="javascript:;" onclick="loadPage(\'article/'+record.id+'/select\');"> <i class="am-icon-pencil"></i> 编辑</a> ';
	        return content;
	    }
	}];

	var dtGridOption = {
	    lang : 'zh-cn',
	    ajaxLoad : true,
	    check : true,
	    checkWidth :'37px',
	    loadURL : 'article/list',
	    columns : dtGridColumns,
	    gridContainer : 'dtGridContainer',
	    toolbarContainer : 'dtGridToolBarContainer',
	    tools : '',
	    pageSize : 10,
	    pageSizeLimit : [10, 20, 30]
	};
	grid = $.fn.dlshouwen.grid.init(dtGridOption);
	grid.load();
	$('select').selected();
	var setting = {
			data: {
				simpleData: {
					enable: true,
					pIdKey: "pid",
				}
			},
			callback: {
				onClick: zTreeOnClick
			}
	};
	 initCategoryTree(setting);
});
function zTreeOnClick(event, treeId, treeNode) {
	console.log(treeNode.id);
	search(treeNode.id);
};
function search(pid){
	  grid.parameters = new Object();
	  grid.parameters['title'] = $("#title").val();
	  if(pid!=1){
		  grid.parameters['category_id'] = pid;
	  }
	  grid.refresh(true);
}

function delteAll(){
	var checkId = getCheckAll(grid.getCheckedRecords());
	if(checkId!=''){
		del("article/"+checkId+"/deleteBatch",search);
	}else{
		 layer.msg('请选择要删除的数据', {
             icon : 3
         });
	}
}
function addArticle(){
	var treeObj = $.fn.zTree.getZTreeObj("zTree");
	var nodes  = treeObj.getSelectedNodes();
	if(nodes.length==0 || nodes[0].id==1){
		layer.msg('请先选择栏目！');
	}else{
		if(nodes[0].type==1 && grid.exhibitDatas.length>0){ //单页
			layer.msg('单页栏目不允许多条数据！');
		}else{
			loadPage('article/form?categoryId='+nodes[0].id);
		}
	}
}

